﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Vidly.Dtos;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity.Migrations;


namespace Vidly.Controllers
{
    public class KomandaController : Controller
    {
        private ApplicationDbContext _context;

        public KomandaController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ViewResult Index()
        {
            if (User.IsInRole(RoleName.CanManageKomanda))
                return View("List");
                
            return View("ReadOnlyList");
        }

        public ViewResult New()
        {
            var genres = _context.Genres.ToList();

            var viewModel = new KomandaFormViewModel
            {
                Genres = genres
            };

            return View("KomandaForm", viewModel);
        }

        
        public ActionResult Edit(int id)
        {
            var komanda = _context.Komanda.SingleOrDefault(c => c.Id == id);
            komanda.nariai = _context.Nariai.ToList<NariaiDTO>();

            if (komanda == null)
                return HttpNotFound();

            var viewModel = new KomandaFormViewModel(komanda)
            {
                Genres = _context.Genres.ToList(),
                Narys1 = komanda.nariai[0].Slapyvardis,
                Narys2 = komanda.nariai[1].Slapyvardis,
                Narys3 = komanda.nariai[2].Slapyvardis,
                Narys4 = komanda.nariai[3].Slapyvardis,
                Narys5 = komanda.nariai[4].Slapyvardis,
            };

            return View("KomandaForm", viewModel);
        }


        public ActionResult Details(int id)
        {
            var komanda = _context.Komanda.SingleOrDefault(m => m.Id == id);

            if (komanda == null)
                return HttpNotFound();

            return View(komanda);

        }

/*
        // GET: Komanda/Random
        public ActionResult Random()
        {
            var komanda = new Komanda() { Name = "Shrek!" };
            var turnyras = new List<Turnyras>
            {
                new Turnyras { Name = "Turnyras 1" },
                new Turnyras { Name = "Turnyras 2" }
            };

            var viewModel = new RandomKomandaViewModel
            {
                Komanda = komanda,
                Turnyras = turnyras
            };

            return View(viewModel);
        }
*/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Komanda komanda, int id)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new KomandaFormViewModel(komanda)
                {
                    Genres = _context.Genres.ToList()
                };

                return View("KomandaForm", viewModel);
            }

            if (komanda.Id != 0)
            {
                
                var newRental = new Rental();
                    var turnyras = _context.Turnyras.Single(
                        c => c.Id == id);
                                
                        if (turnyras.Dalyviu_skaicius == 0)
                            return RedirectToAction("Index", "Turnyras");
                //var NariuSarasas = new List<NariaiDTO>();

                turnyras.Dalyviu_skaicius--;
                var komandadto = new KomandosDto
                {
                    Komandos_Pavadinimas = komanda.Komandos_Pavadinimas,
                    Name = komanda.Name
                };
                
                _context.Komanda.Add(komanda);
                _context.SaveChanges();
                var rental = new Rental
                     {
                         Turnyras = turnyras,
                         Komanda = komanda,
                         DateRented = DateTime.Now
                     };
                
                _context.Rentals.Add(rental);
                _context.SaveChanges();
                var NariuSarasas = new List<NariaiDTO>();
                var nariusar = new List<string>();
                nariusar.Add(komanda.Narys1);
                nariusar.Add(komanda.Narys2);
                nariusar.Add(komanda.Narys3);
                nariusar.Add(komanda.Narys4);
                nariusar.Add(komanda.Narys5);
                foreach(var narys in nariusar)
                {
                    var narysdto = new NariaiDTO()
                    {
                        Slapyvardis = narys,
                        Komanda_Id = komanda.Id
                    };
                    NariuSarasas.Add(narysdto);
                    _context.Nariai.Add(narysdto);
                    _context.SaveChanges();
                }
                return RedirectToAction("Index", "Turnyras");
            }
            return RedirectToAction("Index", "Turnyras");
        }
         
    }
}
