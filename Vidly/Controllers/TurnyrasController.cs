﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class TurnyrasController : Controller
    {
        private ApplicationDbContext _context;

        public TurnyrasController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult New()
        {
            var membershipTypes = _context.MembershipTypes.ToList();
            var viewModel = new TurnyrasFormViewModel
            {
                Turnyras = new Turnyras(),
                MembershipTypes = membershipTypes
            };

            return View("TurnyrasForm", viewModel);
        }

        public ActionResult Register(int id)
        {
            var genres = _context.Genres.ToList();

            var viewModel = new KomandaFormViewModel
            {
                Genres = genres,
                Turnyro_id = id
            };

            return View("KomandaForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Turnyras turnyras)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TurnyrasFormViewModel
                {
                    Turnyras = turnyras,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("TurnyrasForm", viewModel);
            }

            if (turnyras.Id == 0)
                _context.Turnyras.Add(turnyras);
            else
            {
                var turnyrasInDb = _context.Turnyras.Single(c => c.Id == turnyras.Id);
                turnyrasInDb.Name = turnyras.Name;
                turnyrasInDb.Turnyro_pradzia = turnyras.Turnyro_pradzia;
                turnyrasInDb.MembershipTypeId = turnyras.MembershipTypeId;
                turnyrasInDb.Busena = turnyras.Busena;

            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Turnyras");
        }

        public ViewResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            var turnyras = _context.Turnyras.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (turnyras == null)
                return HttpNotFound();

            return View(turnyras);
        }

        public ActionResult Edit(int id)
        {
            var turnyras = _context.Turnyras.SingleOrDefault(c => c.Id == id);

            if (turnyras == null)
                return HttpNotFound();

            var viewModel = new TurnyrasFormViewModel
            {
                Turnyras = turnyras,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("TurnyrasForm", viewModel);
        }
    }
}