﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class KomandaController : ApiController
    {
        private ApplicationDbContext _context;

        public KomandaController()
        {
            _context = new ApplicationDbContext();
        }

        public IEnumerable<KomandosDto> GetKomanda(string query = null)
        {
            var komandaQuery = _context.Komanda
                
                .Where(m => m.Name != null);

            if (!String.IsNullOrWhiteSpace(query))
                komandaQuery = komandaQuery.Where(m => m.Name.Contains(query));

            return komandaQuery
                .ToList()
                .Select(Mapper.Map<Komanda, KomandosDto>);
        }

        public IHttpActionResult GetKomanda(int id)
        {
            var komanda = _context.Komanda.SingleOrDefault(c => c.Id == id);

            if (komanda == null)
                return NotFound();

            return Ok(komanda);
        }

        [HttpPost]
        [Authorize(Roles = RoleName.CanManageKomanda)]
        public IHttpActionResult CreateKomanda(KomandosDto komandaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var komanda = Mapper.Map<KomandosDto, Komanda>(komandaDto);
            _context.Komanda.Add(komanda);
            _context.SaveChanges();

            komandaDto.Id = komanda.Id;
            return Created(new Uri(Request.RequestUri + "/" + komanda.Id), komandaDto);
        }

        [HttpPut]
        [Authorize(Roles = RoleName.CanManageKomanda)]
        public IHttpActionResult UpdateKomanda(int id, KomandosDto komandaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var komandaInDb = _context.Komanda.SingleOrDefault(c => c.Id == id);

            if (komandaInDb == null)
                return NotFound();

            Mapper.Map(komandaDto, komandaInDb);

            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteKomanda(int id)
        {
            var komandaInDb = _context.Komanda.SingleOrDefault(c => c.Id == id);
            pasalintiZaidejusPagalKomanda(id);
            if (komandaInDb == null)
                return NotFound();

            var registracija = _context.Rentals.SingleOrDefault(x => x.Komanda.Id == id);
            _context.Rentals.Remove(registracija);
            _context.Komanda.Remove(komandaInDb);
            _context.SaveChanges();

            return Ok();
        }
        private bool pasalintiZaidejusPagalKomanda(int id)
        {
            var nariai = _context.Nariai.FirstOrDefault(c => c.Komanda_Id == id);
            if (nariai == null)
                return false;
            while (nariai != null && nariai.Komanda_Id == id)
            {
                var narys = _context.Nariai.FirstOrDefault(c => c.Komanda_Id == id);
                if (narys != null)
                {
                    _context.Nariai.Remove(narys);
                    _context.SaveChanges();
                }
                else
                {
                    nariai = null;
                }

            }
            return true;
        }
    }
}
