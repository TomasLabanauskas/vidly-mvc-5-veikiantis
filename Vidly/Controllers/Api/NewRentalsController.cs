﻿using System;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();    
        }

        [HttpPost]
        public IHttpActionResult CreateNewRentals(NewRentalDto newRental)
        {
            var turnyras = _context.Turnyras.Single(
                c => c.Id == newRental.CustomerId);

            var komandos = _context.Komanda.Where(
                m => newRental.KomandaIds.Contains(m.Id)).ToList();

            foreach (var komanda in komandos)
            {
                if (turnyras.Dalyviu_skaicius == 0)
                    return BadRequest("Komanda is not available.");

                turnyras.Dalyviu_skaicius--;
                
                var rental = new Rental
                {
                    Turnyras = turnyras,
                    Komanda = komanda,
                    DateRented = DateTime.Now
                };

                _context.Rentals.Add(rental);
            }

            _context.SaveChanges();

            return Ok();
        }
        public void CreateRentals(NewRentalDto newRental)
        {
            CreateNewRentals(newRental);
        }
    }
}
