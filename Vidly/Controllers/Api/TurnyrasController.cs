﻿using AutoMapper;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class TurnyrasController : ApiController
    {
        private ApplicationDbContext _context;

        public TurnyrasController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/turnyras
        public IHttpActionResult GetTurnyras(string query = null)
        {
            var turnyrasQuery = _context.Turnyras
                .Include(c => c.MembershipType);

            if (!String.IsNullOrWhiteSpace(query))
                turnyrasQuery = turnyrasQuery.Where(c => c.Name.Contains(query));

            var turnyrasDtos = turnyrasQuery
                .ToList()
                .Select(Mapper.Map<Turnyras, TurnyroDto>);
            
            return Ok(turnyrasDtos);    
        }

        // GET /api/turnyras/1
        public IHttpActionResult GetTurnyras(int id)
        {
            var turnyras = _context.Turnyras.SingleOrDefault(c => c.Id == id);

            if (turnyras == null)
                return NotFound();

            return Ok(Mapper.Map<Turnyras, TurnyroDto>(turnyras));
        }

        // POST /api/turnyras
        [HttpPost]
        public IHttpActionResult CreateCustomer(TurnyroDto turnyrasDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var turnyras = Mapper.Map<TurnyroDto, Turnyras>(turnyrasDto);
            _context.Turnyras.Add(turnyras);
            _context.SaveChanges();

            turnyrasDto.Id = turnyras.Id;
            return Created(new Uri(Request.RequestUri + "/" + turnyras.Id), turnyrasDto);
        }

        // PUT /api/turnyras/1
        [HttpPut]
        public IHttpActionResult UpdateCustomer(int id, TurnyroDto turnyrasDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var turnyrasInDb = _context.Turnyras.SingleOrDefault(c => c.Id == id);

            if (turnyrasInDb == null)
                return NotFound();

            Mapper.Map(turnyrasDto, turnyrasInDb);

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/turnyras/1
        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var turnyrasInDb = _context.Turnyras.SingleOrDefault(c => c.Id == id);

            if (turnyrasInDb == null)
                return NotFound();
            rastiKomanda(id);
            _context.Turnyras.Remove(turnyrasInDb);
            _context.SaveChanges();

            return Ok();
        }
        private void rastiKomanda(int id)
        {
            var registracija = _context.Rentals.SingleOrDefault(x => x.Turnyras.Id == id);
            var komandos = registracija.Komanda;
            while (komandos != null && komandos.Id == id)
            {
                var komanda = _context.Komanda.FirstOrDefault(c => c.Id == komandos.Id);
                if (komanda != null)
                {
                    DeleteKomanda(komanda.Id);
                }
                else
                {
                    komandos = null;
                }

            }

        }
        private bool pasalintiZaidejusPagalKomanda(int id)
        {
            var nariai = _context.Nariai.FirstOrDefault(c => c.Komanda_Id == id);
            if (nariai == null)
                return false;
            while (nariai != null && nariai.Komanda_Id == id)
            {
                var narys = _context.Nariai.FirstOrDefault(c => c.Komanda_Id == id);
                if (narys != null)
                {
                    _context.Nariai.Remove(narys);
                    _context.SaveChanges();
                }
                else
                {
                    nariai = null;
                }

            }
            return true;
        }
        public void DeleteKomanda(int id)
        {
            var komandaInDb = _context.Komanda.SingleOrDefault(c => c.Id == id);
            pasalintiZaidejusPagalKomanda(id);
                   
            var registracija = _context.Rentals.SingleOrDefault(x => x.Komanda.Id == id);
            _context.Rentals.Remove(registracija);
            _context.Komanda.Remove(komandaInDb);
            _context.SaveChanges();

            
        }
    }
}
