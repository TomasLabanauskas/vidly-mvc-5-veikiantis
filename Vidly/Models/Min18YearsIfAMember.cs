﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var turnyras = (Turnyras)validationContext.ObjectInstance;

            var age = 20;

            return (age >= 18) 
                ? ValidationResult.Success 
                : new ValidationResult("Turnyras should be at least 18 years old to go on a membership.");
        }
    }
}