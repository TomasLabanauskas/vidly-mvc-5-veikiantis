﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Turnyras
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public int Dalyviu_skaicius { get; set; }
        public int Komandos_dydis { get; set; }
        public string Aprasymas { get; set; }
        public bool Busena { get; set; }
        [Display(Name = "Žaidimas")]
        public MembershipType MembershipType { get; set; }

        [Display(Name = "Žaidimas")]
        public byte MembershipTypeId { get; set; }

        [Display(Name = "Turnyro_pradzia")]
        public DateTime? Turnyro_pradzia { get; set; }
    }
}