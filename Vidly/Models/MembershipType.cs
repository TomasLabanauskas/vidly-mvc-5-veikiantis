﻿using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class MembershipType
    {
        public byte Id { get; set; }
        [Required]
        public string Name { get; set; }
        public short SignUpFee { get; set; }
        public byte DurationInMonths { get; set; }
        public byte DiscountRate { get; set; }

        public static readonly byte Counter_Strike = 1;
        public static readonly byte LOL = 2;
        public static readonly byte Zelda = 3;
        public static readonly byte wow = 4;
    }
}