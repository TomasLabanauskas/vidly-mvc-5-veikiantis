﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vidly.Dtos;

namespace Vidly.Models
{
    public class Komanda
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        
        public string Name { get; set; }
        public string Komandos_Pavadinimas { get; set; }
        public List<NariaiDTO> nariai { get; set; }
        [NotMapped]
        public string Narys1 { get; set; }
        [NotMapped]
        public string Narys2 { get; set; }
        [NotMapped]
        public string Narys3 { get; set; }
        [NotMapped]
        public string Narys4 { get; set; }
        [NotMapped]
        public string Narys5 { get; set; }
    }
}