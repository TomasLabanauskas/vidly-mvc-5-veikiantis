namespace Vidly.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class SetNameOfMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE MembershipTypes SET Name = 'Counter_Strike' WHERE Id = 1");
            Sql("UPDATE MembershipTypes SET Name = 'LOL' WHERE Id = 2");
            Sql("UPDATE MembershipTypes SET Name = 'Zelda' WHERE Id = 3");
            Sql("UPDATE MembershipTypes SET Name = 'wow' WHERE Id = 4");
            
    }
        
        public override void Down()
        {
        }
    }
}
