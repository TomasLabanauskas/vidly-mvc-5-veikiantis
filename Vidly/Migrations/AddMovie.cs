namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKomanda : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Komanda",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 255),
                    Komandos_Pavadinimas = c.String()

                })
                .PrimaryKey(t => t.Id);
            AddColumn("dbo.Nariai", "KomandosId", c => c.Int(nullable: false));
            CreateIndex("dbo.Nariai", "KomandosId");
            AddForeignKey("dbo.Nariai", "KomandosId", "dbo.Komanda", "Id", cascadeDelete: true);


            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Rentals",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    DateRented = c.DateTime(nullable: false),
                    DateReturned = c.DateTime(),
                    Turnyras_Id = c.Int(nullable: false),
                    Komanda_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Turnyras_Id)
                .Index(t => t.Komanda_Id);
            AddForeignKey("dbo.Rentals", "Turnyras_Id", "dbo.Turnyras", "Id");
            AddForeignKey("dbo.Rentals", "Komanda_Id", "dbo.Komanda", "Id");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Komanda", "GenreId", "dbo.Genres");
            DropIndex("dbo.Komanda", new[] { "GenreId" });
            DropTable("dbo.Genres");
            DropTable("dbo.Komanda");
            DropForeignKey("dbo.Rentals", "Komanda_Id", "dbo.Komanda");
            DropForeignKey("dbo.Rentals", "Turnyras_Id", "dbo.Turnyras");
            DropIndex("dbo.Rentals", new[] { "Komanda_Id" });
            DropIndex("dbo.Rentals", new[] { "Turnyras_Id" });
            DropTable("dbo.Rentals");
        }
    }
}
