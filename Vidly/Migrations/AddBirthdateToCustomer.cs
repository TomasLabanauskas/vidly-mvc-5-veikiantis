namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBirthdateToCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Turnyras", "Turnyro_pradzia", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Turnyras", "Turnyro_pradzia");
        }
    }
}
