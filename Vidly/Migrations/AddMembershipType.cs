namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MembershipTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        SignUpFee = c.Short(nullable: false),
                        DurationInMonths = c.Byte(nullable: false),
                        DiscountRate = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Turnyras", "MembershipTypeId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Turnyras", "MembershipTypeId");
            AddForeignKey("dbo.Turnyras", "MembershipTypeId", "dbo.MembershipTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Turnyras", "MembershipTypeId", "dbo.MembershipTypes");
            DropIndex("dbo.Turnyras", new[] { "MembershipTypeId" });
            DropColumn("dbo.Turnyras", "MembershipTypeId");
            DropTable("dbo.MembershipTypes");
        }
    }
}
