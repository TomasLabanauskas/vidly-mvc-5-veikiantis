namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRental : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rentals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateRented = c.DateTime(nullable: false),
                        DateReturned = c.DateTime(),
                        Turnyras_Id = c.Int(nullable: false),
                        Komanda_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Turnyras_Id)
                .Index(t => t.Komanda_Id);
            AddForeignKey("dbo.Rentals", "Turnyras_Id", "dbo.Turnyras", "Id");
            AddForeignKey("dbo.Rentals", "Komanda_Id", "dbo.Komanda", "Id");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "Komanda_Id", "dbo.Komanda");
            DropForeignKey("dbo.Rentals", "Turnyras_Id", "dbo.Turnyras");
            DropIndex("dbo.Rentals", new[] { "Komanda_Id" });
            DropIndex("dbo.Rentals", new[] { "Turnyras_Id" });
            DropTable("dbo.Rentals");
        }
    }
}
