﻿using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Dto
            Mapper.CreateMap<Turnyras, TurnyroDto>();
            Mapper.CreateMap<Komanda, KomandosDto>();
            Mapper.CreateMap<MembershipType, MembershipTypeDto>();
            Mapper.CreateMap<Genre, GenreDto>();


            // Dto to Domain
            Mapper.CreateMap<TurnyroDto, Turnyras>()
                .ForMember(c => c.Id, opt => opt.Ignore());

            Mapper.CreateMap<KomandosDto, Komanda>()
                .ForMember(c => c.Id, opt => opt.Ignore());
        }
    }
}