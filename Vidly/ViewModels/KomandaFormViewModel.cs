﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class KomandaFormViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }

        public int? Id { get; set; }

        [Required]
        [StringLength(255)]

        public string Name { get; set; }
        public string Komandos_Pavadinimas { get; set; }
        public string Narys1 { get; set; }
        public string Narys2 { get; set; }
        public string Narys3 { get; set; }
        public string Narys4 { get; set; }
        public string Narys5 { get; set; }
        public int Turnyro_id { get; set; }


        public string Title
        {
            get
            {
                return Id != 0 ? "Edit Komanda" : "New Komanda";
            }
        }

        public KomandaFormViewModel()
        {
            Id = 0;
        }

        public KomandaFormViewModel(Komanda komanda)
        {
            Id = komanda.Id;
            Name = komanda.Name;
            Komandos_Pavadinimas = komanda.Komandos_Pavadinimas;
            Narys1 = Narys1;
            Narys2 = Narys2;
            Narys3 = Narys3;
            Narys4 = Narys4;
            Narys5 = Narys5;
         


        }
    }
}