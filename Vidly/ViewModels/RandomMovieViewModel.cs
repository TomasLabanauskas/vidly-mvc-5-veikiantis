﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class RandomKomandaViewModel
    {
        public Komanda Komanda { get; set; }
        public List<Turnyras> Turnyras { get; set; }
    }
}