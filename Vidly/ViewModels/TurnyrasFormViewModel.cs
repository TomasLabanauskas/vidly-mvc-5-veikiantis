﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class TurnyrasFormViewModel
    {
        [Display(Name = "Žaidimas")]
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Turnyras Turnyras { get; set; }
    }
}