﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Dtos
{
    public class KomandosDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public string Komandos_Pavadinimas {get ; set; }
        public IEnumerable<NariaiDTO> Nariai { get; set; }
              
    }
}