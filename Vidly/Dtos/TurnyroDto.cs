﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Dtos
{
    public class TurnyroDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name {get; set; }
        public string Aprasymas { get; set; }
        public bool Busena { get; set; }

        public byte MembershipTypeId { get; set; }

        public int Dalyviu_skaicius { get; set; }

        public int Komandos_dydis { get; set; }
        //        [Min18YearsIfAMember]
        public DateTime? Turnyro_pradzia { get; set; }
    }

}