﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class NariaiDTO
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Slapyvardis { get; set; }
        public int Komanda_Id { get; set; }

    }
}